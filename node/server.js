const express = require("express")
const app = express()
const bodyParser = require("body-parser")
const torrentStream = require("torrent-stream")
const OpenSubtitles = require("opensubtitles-api")
var request = require("request")
const fs = require("fs")
var srt2vtt = require("srt2vtt")

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

var port = 5000
app.listen(port, () => console.log(`Server running on port ${port}`))

var pathToFile
var pathCopy
var en
var ru


app.post("/", (req, res) => {
    var { hash, id, resolution } = req.body
console.log(resolution)
    hash = hash.toLowerCase()
    id = id.substr(2)


    const filePathExists = lookForFile(hash)
    if (!filePathExists) {
        try {
            launchTorrentStream(hash, id)
        } catch (err) {
            console.log(err)
            res.setHeader("Access-Control-Allow-Origin", "http://localhost:8100")
            res.writeHead(500, { "Content-Type": "application/json" })
            return res.end(JSON.stringify({ error: `Something's wrong, try again` }))
        }
    } else {
        let path = null
        filePathExists[0].files.forEach(file => {
            const ext = file.path ? file.path.split(".").pop() : undefined
            if (
                ext === "mp4" ||
                ext === "avi" ||
                ext === "mkv" ||
                ext === "webm" ||
                ext === "flv" ||
                ext === "m4v"
            ) {
                path = file.path
            }
        })

        if (path.search(resolution) !== -1) {
            try {
                const pathToSubs = path.substr(4)
                res.setHeader("Access-Control-Allow-Origin", "http://localhost:8100")
                res.writeHead(200, { "Content-Type": "application/json" })
                return res.end(
                    JSON.stringify({
                        link: path ? path : "something went wrong",
                        hashFolder: `\/tmp\/torrent-stream\/${hash}`,
                        en: `${pathToSubs}.en.vtt`,
                        ru: `${pathToSubs}.ru.vtt`
                    })
                )
            } catch (err) {
                console.log(err)
            }

        } else {
            try {
                launchTorrentStream(hash, id)
            } catch (err) {
                console.log(err)
                res.setHeader("Access-Control-Allow-Origin", "http://localhost:8100")
                res.writeHead(500, { "Content-Type": "application/json" })
                return res.end(JSON.stringify({ error: `Something's wrong, try again` }))
            }
        }
    }

    waitForPath()

    function waitForPath() {
        if (
            typeof pathToFile !== "undefined" &&
            typeof pathCopy !== "undefined" &&
            pathCopy === pathToFile
        ) {
            try {
                res.setHeader("Access-Control-Allow-Origin", "http://localhost:8100")
                res.writeHead(200, { "Content-Type": "application/json" })
                res.end(
                    JSON.stringify({
                        link: pathToFile ? pathToFile : "Something's wrong, try again",
                        hashFolder: `\/tmp\/torrent-stream\/${hash}`,
                        en,
                        ru
                    })
                )
                pathToFile = undefined
                pathCopy = undefined
                en = undefined
                ru = undefined
                return
            } catch (err) {
                console.log(err)
            }
        } else {
            setTimeout(waitForPath, 100)
        }
    }
})

launchTorrentStream = (hash, imdbid) => {
    const openSub = new OpenSubtitles("TemporaryUserAgent")

    var engine = torrentStream(hash)

    engine.on("ready", () => {
        var movieIndex = 0

        for (var i = 1; i < engine.files.length; i++) {
            if (engine.files[i].length > engine.files[movieIndex].length) {
                movieIndex = i
            }
        }

        const vidos = engine.files[movieIndex]

        vidos.createReadStream()
        pathCopy = `\/tmp\/torrent-stream\/${hash}\/${vidos.path}`

        if (!fs.existsSync(`..\/public\/torrent-stream`)) {
            fs.mkdirSync(`..\/public\/torrent-stream`)
        }

        if (!fs.existsSync(`..\/public\/torrent-stream\/${hash}`)) {
            fs.mkdirSync(`..\/public\/torrent-stream\/${hash}`)
        }

        if (
            !fs.existsSync(
                `..\/public\/torrent-stream\/${hash}\/${engine.torrent.name}`
            )
        ) {
            fs.mkdirSync(
                `..\/public\/torrent-stream\/${hash}\/${engine.torrent.name}`
            )
        }

        pathToSubs = `..\/public\/torrent-stream\/${hash}\/${vidos.path}`

        openSub
            .search({ imdbid })
            .then(async subs => {
                if (subs.en) {
                    const file = fs.createWriteStream(`${pathToSubs}.en.srt`)
                    await request(subs.en.url)
                        .pipe(file)
                        .on("finish", function() {
                            var srtData = fs.readFileSync(pathToSubs + ".en.srt")
                            srt2vtt(srtData, function(err, vttData) {
                                if (err) console.log(err)
                                fs.writeFileSync(pathToSubs + ".en.vtt", vttData)
                                en = pathToSubs.substr(9) + ".en.vtt"
                            })
                        })
                }
                if (subs.ru) {
                    const file = fs.createWriteStream(`${pathToSubs}.ru.srt`)
                    request(subs.ru.url)
                        .pipe(file)
                        .on("finish", function() {
                            var srtData = fs.readFileSync(pathToSubs + ".ru.srt")
                            srt2vtt(srtData, function(err, vttData) {
                                if (err) console.log(err)
                                fs.writeFileSync(pathToSubs + ".ru.vtt", vttData)
                                ru = pathToSubs.substr(9) + ".ru.vtt"
                            })
                        })
                }
            })
            .catch(err => console.log(err))

        engine.on("download", () => {
            const progress = (engine.swarm.downloaded / vidos.length) * 100
            console.log(
                `dowloading torrent ... ${hash} -> ` +
                ((engine.swarm.downloaded / vidos.length) * 100).toFixed(1) +
                " %"
            )
            if (progress >= 2.1) {
                pathToFile = `\/tmp\/torrent-stream\/${hash}\/${vidos.path}`
            }
        })
    })
}

lookForFile = hash => {
    const dir = `\/tmp\/torrent-stream`

    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir)
    }

    const path = require("path")
    var bingoFolder = null

    fs.readdirSync(dir).forEach(folder => {
        if (folder.lastIndexOf(".") === -1 && hash === folder) {
            bingoFolder = folder
        }
    })

    if (!bingoFolder) {
        return false
    }

    const recurseCheck = (
        dir = `\/tmp\/torrent-stream\/${bingoFolder}`,
        result = []
    ) => {
        fs.readdirSync(dir).forEach(file => {
            const fPath = path.resolve(dir, file)
            const contents = { file, path: fPath }

            if (fs.statSync(fPath).isDirectory()) {
                contents.type = "dir"
                contents.files = []
                result.push(contents)
                return recurseCheck(fPath, contents.files)
            }
            contents.type = "file"
            result.push(contents)
        })
        return result
    }
    return recurseCheck()
}
