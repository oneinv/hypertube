<?php
require_once 'Api.php';
require(ROOT . '/Components/Db.php');
require(ROOT . '/Models/Users.php');

class UsersApi extends Api
{
    public $apiName = 'users';

    private $allowedKeys = array('login', 'email', 'name', 'lastname', 'password');

    public function indexAction()
    {
        $users = Users::getAllUsers();
        if($users){
            return $this->response($users, 200);
        }

        return $this->response($this->responseGenerator('Users not found', 404), 404);
    }

    public function viewAction()
    {
        $data = array();
    		foreach ($this->requestParams as $key => $value) {
      			$data[strtolower($key)] = strtolower($value);
      			if (!in_array(strtolower($key),$this->allowedKeys))
      				  return $this->response($this->responseGenerator('Unknown parametr', 404), 404);
    		}
        if (!$response = $this->validationUpdate($data, true)) {
            if($user = Users::getUserByLogin($data['login'])) {
                return $this->response($user, 200);
            } else {
				        return $this->response($this->responseGenerator('User not found', 404), 404);
            }
        } else {
            return $this->response($this->responseGenerator($response, 400), 400);
        }
    }

    public function createAction()
    {
        $data = array();

        foreach ($this->requestParams as $key => $value) {
                $data[strtolower($key)] = $value;
                if (!in_array(strtolower($key),$this->allowedKeys))
                    return $this->response($this->responseGenerator('Unknown parametr', 404), 404);
        }

        if (!$response = $this->validationCreate($data)) {
            if (Users::create($data)) {
                return $this->response(
                               $this->responseGenerator("User ".$data['login']." created", 200), 200);
            } else {
                    return $this->response(
                               $this->responseGenerator("Smthng Wrong", 500), 500);
              }
        } else {
              return $this->response($this->responseGenerator($response, 400), 400);
        }

    }

    public function updateAction()
    {
        $data = array();

        foreach ($this->requestParams as $key => $value) {
            $data[strtolower($key)] = $value;
            if (!in_array(strtolower($key),$this->allowedKeys))
                return $this->response($this->responseGenerator('Unknown parametr', 404), 404);
        }


        if (!$response = $this->validationUpdate($data)) {
//
            if (isset($data['email']) && Users::emailExists($data['email'])) {
                if (Users::update($data)) {
                    return $this->response(
                        $this->responseGenerator("User ".$data['email']." updated!", 200), 200);
                } else {
                    return $this->response(
                        $this->responseGenerator("Smthng Wrong", 500), 500);
                }
            } else {
                return $this->response(
                    $this->responseGenerator('please enter valid user email', 500), 500);
            }
        } else {
            return $this->response($this->responseGenerator($response, 400), 400);
        }
    }

    public function deleteAction()
    {
        $data = array();
        foreach ($this->requestParams as $key => $value) {
            $data[strtolower($key)] = $value;
            if (!in_array(strtolower($key),$this->allowedKeys))
                return $this->response($this->responseGenerator('Unknown parametr', 404), 404);
        }

        if (isset($data['email']) && Users::emailExists($data['email'])) {
            if (Users::delete($data)) {
				       return $this->response(
                	    $this->responseGenerator("User ".$data['email']." deleted!", 200), 200);
            } else {
                return $this->response(
                     $this->responseGenerator("Smthng Wrong", 500), 500);
            }
        } else {
            return $this->response(
                 $this->responseGenerator('please enter valid user email', 500), 500);
        }
    }

    private function validationCreate($data)
    {
    		if (!strlen($data['name']))
    			 return "invalid value in field 'name'";
    		if (!strlen($data['lastname']))
    			 return "invalid value in field 'lastname'";
    		if (Users::nameExists($data['login']))
    			 return "This login already exists";
        if (Users::emailExists($data['email']))
        	 return "This email already exists";
    		return false;
    }

    private function validationUpdate($data, $get = false)
    {
    		if (isset($data['name'])) {
      			if (!strlen($data['name']))
      				return "invalid value in field 'name'";
    		}
    		if (isset($data['lastname'])) {
      			if (!strlen($data['lastname']))
      				return "invalid value in field 'lastname'";
    		}

      	return false;
    }
}
