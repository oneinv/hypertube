<?php

define('ROOT', dirname(__DIR__, 1) . '/api');

if (strrpos($_SERVER['REDIRECT_URL'], '/api/users') === 0) {
    require_once 'UsersApi.php';
    $apiClass = new usersApi();
}
elseif (strrpos($_SERVER['REDIRECT_URL'], '/api/movies') === 0) {
    require_once 'MoviesApi.php';
    $apiClass = new moviesApi();
}

if ($apiClass) {
    try {
        $api = new $apiClass();
        echo $api->run();
    } catch (Exception $e) {
        echo json_encode(Array('error' => $e->getMessage()));
    }
}
else
    echo "Enter correct api name, smth like 'users' or 'movies' ";
