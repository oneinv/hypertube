-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3307
-- Generation Time: Apr 15, 2019 at 09:21 AM
-- Server version: 8.0.15
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `api`
--

-- --------------------------------------------------------

--
-- Table structure for table `all_visits`
--

CREATE TABLE `all_visits` (
  `id` int(10) NOT NULL,
  `ip` varchar(10) NOT NULL,
  `date` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table structure for table `authors`
--

CREATE TABLE `authors` (
  `author_id` int(55) NOT NULL,
  `author_name` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `b_day` int(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`author_id`, `author_name`, `b_day`) VALUES
(1, 'King', 1947),
(2, 'Tolkien', 1892),
(3, 'Sapkowskiy', 1942),
(4, 'Martin', 1948);

-- --------------------------------------------------------

--
-- Table structure for table `black_list_ip`
--

CREATE TABLE `black_list_ip` (
  `id` int(11) NOT NULL,
  `ip` int(11) NOT NULL,
  `date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `author` varchar(55) NOT NULL,
  `name` varchar(55) NOT NULL,
  `theme` varchar(55) NOT NULL,
  `pages` int(55) NOT NULL,
  `year` int(55) NOT NULL,
  `price` float NOT NULL,
  `isbn` int(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `author`, `name`, `theme`, `pages`, `year`, `price`, `isbn`) VALUES
(1, 'King', 'Dark_tower', 'Phantasy', 10, 1964, 100, 1234),
(2, 'King', 'Dark_tower', 'Phantasy', 10, 1990, 99, 12345),
(3, 'King', 'Test', 'Test', 1111, 1955, 125, 1234567),
(4, 'King', 'Dark_tower', 'Phantasy', 10, 1293, 123, 1),
(5, 'King', 'Dark_tower', 'Phantasy', 10, 1293, 123, 1),
(6, 'King', 'Dark_tower', 'Phantasy', 10, 1293, 123, 123456),
(7, 'King', 'Dark_tower', 'Phantasy', 10, 1293, 123, 123456),
(8, 'King', 'Dark_tower', 'Phantasy', 10, 1293, 123, 123456),
(9, 'King', 'Dark_tower', 'Phantasy', 10, 1293, 123, 1),
(10, 'King', 'Dark_tower', 'Phantasy', 10, 1293, 123, 4),
(11, 'King', 'Dark_tower', 'Phantasy', 10, 1293, 123, 4);

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE `links` (
  `author_id` int(55) NOT NULL,
  `theme_id` int(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `links`
--

INSERT INTO `links` (`author_id`, `theme_id`) VALUES
(1, 1),
(1, 3),
(1, 4),
(2, 1),
(3, 1),
(3, 5),
(4, 1),
(4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE `themes` (
  `theme_id` int(55) NOT NULL,
  `theme_name` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`theme_id`, `theme_name`) VALUES
(1, 'fantasy'),
(2, 'science'),
(3, 'horror'),
(4, 'thrill '),
(5, 'scify');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `all_visits`
--
ALTER TABLE `all_visits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`author_id`);

--
-- Indexes for table `black_list_ip`
--
ALTER TABLE `black_list_ip`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `links`
--
ALTER TABLE `links`
  ADD KEY `authors_link` (`author_id`),
  ADD KEY `themes_link` (`theme_id`);

--
-- Indexes for table `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`theme_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `all_visits`
--
ALTER TABLE `all_visits`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=922;

--
-- AUTO_INCREMENT for table `authors`
--
ALTER TABLE `authors`
  MODIFY `author_id` int(55) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `black_list_ip`
--
ALTER TABLE `black_list_ip`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `themes`
--
ALTER TABLE `themes`
  MODIFY `theme_id` int(55) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `links`
--
ALTER TABLE `links`
  ADD CONSTRAINT `authors_link` FOREIGN KEY (`author_id`) REFERENCES `authors` (`author_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `themes_link` FOREIGN KEY (`theme_id`) REFERENCES `themes` (`theme_id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
