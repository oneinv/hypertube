<?php
require_once 'Api.php';

require(ROOT . '/Models/Movies.php');

class MoviesApi extends Api
{
    public $apiName = 'movies';

    private $allowedKeys = array('name');

    public function indexAction()
    {
        if($movies = Movies::getPopularMovies()) {
            return $this->response($movies, 200);
        }

        return $this->response(
			      $this->responseGenerator('No movies for you ;(', 404), 404);
    }

    public function viewAction()
    {
        $data = array();
		    foreach ($this->requestParams as $key => $value) {
      			$data[strtolower($key)] = strtolower($value);
      			if (!in_array(strtolower($key),$this->allowedKeys))
      			   return $this->response($this->responseGenerator('Unknown parametr', 404), 404);
      	}

        if (!$response = $this->validationUpdate($data)) {
		        if (isset($data['name']) && ($movies = Movies::getMovieByName($data['name']))) {
                return $this->response($movies, 200);
            } else {
        			  return $this->response(
        				      $this->responseGenerator('Movie ' . $data['name'] ." not found" , 404), 404);
        		}
	      } else {
		        return $this->response($response, 500);
	      }
    }

    public function createAction()
    {
        //
    }

    public function updateAction()
    {
        //
    }

    public function deleteAction()
    {
        //
    }

  	private function validationUpdate($data, $get = false)
    {
    		if (isset($data['name'])) {
    		    if (!strlen($data['name']))
    				    return "invalid value in field 'name'";
    		return false;
        }
        return false;
    }
}
