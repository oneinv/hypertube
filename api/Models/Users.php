<?php


require_once(ROOT . '/Components/Db.php');
require_once(ROOT . '/Components/YTS.php');


class Users
{
		public static function getAllUsers() {

				$db = Db::getConnection();
				$sql = "SELECT * FROM users";
				$result =$db->prepare($sql);
				$result->execute();
				$i = 0;
				while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
		    		$users[$i] = $row;
						$i++;
				}
				return $users;
		}

		public function getUserByLogin($login)
		{
				$db = Db::getConnection();
				$sql = "SELECT * FROM users
			 			WHERE login=:login";
				$result = $db->prepare($sql);
				$result->bindParam(':login', $login, PDO::PARAM_STR);
				$result->execute();
				$book = $result->fetch(PDO::FETCH_ASSOC);

				return $book;
		}

        public static function check_long(&$data)
        {
            if (strlen($data) >= 20) {
                return false;
            }
            return true;
        }
		public static function create($data)
		{
				$db = Db::getConnection();

                foreach ($data as $value) {
                    if (strlen($value) >= 200) {
                        return false;
                    }
                }

				if (self::emailExists($data['email']) || self::nameExists(data['login']))
				    return false;

				$token = bin2hex(random_bytes(10));
				$password = password_hash($data['password'], PASSWORD_DEFAULT);

				$sql = 'INSERT INTO users (login, email, password, name, lastname, token) VALUES (:login, :email, :password, :name, :lastname, :token)';

				$result = $db->prepare($sql);
				$result->bindParam(':login', $data['login'], PDO::PARAM_STR);
				$result->bindParam(':email', $data['email'], PDO::PARAM_STR);
				$result->bindParam(':password', $password, PDO::PARAM_STR);
				$result->bindParam(':name', $data['name'], PDO::PARAM_STR);
				$result->bindParam(':lastname', $data['lastname'], PDO::PARAM_STR);
				$result->bindParam(':token', $token, PDO::PARAM_STR);

				$id = self::getId($data['login']);
                $path = 'Users/yskorode/yskorode/hypertube/public/uploads/users/';

                if (!file_exists($path.$id))
                    mkdir($path.$id);

				return $result->execute();
		}

		public static function update($data)
		{
            foreach ($data as $value) {
                if (strlen($value) >= 200) {
                    return false;
                }
            }

            $db = Db::getConnection();
            $sql = 'UPDATE users SET ' . self::formatUpdate($data) . " WHERE email=:email";
            $result = $db->prepare($sql);
            $result->bindParam(':email', $data['email'], PDO::PARAM_STR);

            return $result->execute();
		}

		public static function delete($data)
		{
            foreach ($data as $value) {
                if (strlen($value) >= 200) {
                    return false;
                }
            }

            $db = Db::getConnection();
            $sql = "DELETE FROM users WHERE email=:email";
            $result = $db->prepare($sql);
            $result->bindParam(':email', $data['email'], PDO::PARAM_STR);

            return $result->execute();
		}

		public static function formatUpdate($sql)
		{
            $i = 0;
            $str = '';
            $len = count($sql) - 1;
            foreach ($sql as $key => $value) {
                    if ($len == $i) {
                        $str .=  $key . '=' . "'" . $value. "' ";
                    } else {
                        $str .=  $key . '=' . "'" . $value. "', ";
                    }
                    $i++;
            }
            return $str;
		}

	public static function formatView($sql, $count = false)
	{
			$i = 0;
			$str = '';
			foreach ($sql as $key => $value) {
					if ($i < 1)
						$str .= 'WHERE ' . $key . '=' . "'" . $value. "' ";
					if ($i >= 1)
						$str .= 'AND ' . $key . '=' . "'" . $value. "' ";
					$i++;
			}
			return $str;
	}

		public static function emailExists($email)
		{
            $db = Db::getConnection();
            $sql = "SELECT COUNT(*) FROM users WHERE email=:email";
            $result = $db->prepare($sql);
            $result->bindParam(':email', $email, PDO::PARAM_STR);
            $result->execute();
            $res = $result->fetch();

            return ($res[0]);
		}

		public static function nameExists($login)
		{
            $db = Db::getConnection();
            $sql = "SELECT COUNT(*) FROM users WHERE login=:login";
            $result = $db->prepare($sql);
            $result->bindParam(':login', $login, PDO::PARAM_STR);
            $result->execute();
            $res = $result->fetch();

            return ($res[0]);
		}

		public static function getId($login)
		{
			 $db = Db::getConnection();
			 $sql = 'SELECT id from users WHERE login =:login';
			 $result = $db->prepare($sql);
			 $result->bindParam(':login', $login, PDO::PARAM_STR);
			 $result->execute();
			 $res = $result->fetch(PDO::FETCH_ASSOC);

			 return $res['id'];
		}

}
