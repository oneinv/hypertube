<?php

require_once(ROOT . '/Components/Db.php');
require_once(ROOT . '/Components/YTS.php');
//var_dump(ROOT);
use PDO;


class Movies
{
		public function getPopularMovies()
		{
            $list = [];
            $i = 0;

		    $yts = new \App\Components\YTS();
		    $movies = $yts->listMovies('all', 50);

//            if (!$movies) {
//                return array(
//                    "error" => array(
//                        "error_msg" => "Api Not Available.",
//                        "error_code" => 400
//                     )
//                );
//            }
		    foreach ($movies as $movie) {
		        $list[$i]['title'] = (string)$movie->title;
		        $list[$i]['rating'] = (float)$movie->rating;
		        $list[$i]['synopsis'] = $movie->synopsis;
//		        if (strlen($list[$i]['synopsis']) >= 150) {
//		            $list[$i]['synopsis'] = substr($list[$i]['synopsis'], 0, 150);
//		            $list[$i]['synopsis']= substr_replace($list[$i]['synopsis'], "...", 150);
//		        }
		        $list[$i]['poster'] = $movie->medium_cover_image;
		        $list[$i]['year'] = $movie->year;
		        $list[$i]['imdb_code'] = $movie->imdb_code;
		        foreach ($movie->torrents as $key => $value) {
		            $list[$i][$value->quality] = (string)$value->hash;
		        }
		        $i++;
		    }
		    array_multisort(array_column($list, 'rating'), SORT_DESC, $list);

            return $list;
        }

		public function getMovieByName($name)
		{
            $list = array();
			  $title = strtr($name, [' ' => '%20']);

			  $yts = new \App\Components\YTS();

			  $movies = $yts->listMovies('all', 50, $title);
//				if (!$movies) {
//						return array(
//							"error" => array(
//								"error_msg" => "Api Not Available.",
//								"error_code" => 400
//							 )
//					 	);
//				}
            $i = 0;
            $list = [];
            foreach ($movies as $movie) {
                $list[$i]['title'] = (string)$movie->title;
                $list[$i]['rating'] = (float)$movie->rating;
                $list[$i]['synopsis'] = $movie->synopsis;
//                if (strlen($list[$i]['synopsis']) >= 150) {
//                    $list[$i]['synopsis'] = substr($list[$i]['synopsis'], 0, 150);
//                    $list[$i]['synopsis']= substr_replace($list[$i]['synopsis'], "...", 150);
//                }
                $list[$i]['poster'] = $movie->medium_cover_image;
                $list[$i]['year'] = $movie->year;
                $list[$i]['imdb_code'] = $movie->imdb_code;
                foreach ($movie->torrents as $key => $value) {
                    $list[$i][$value->quality] = (string)$value->hash;
                }
                $i++;
            }
				array_multisort(array_column($list, 'rating'), SORT_DESC, $list);

				return $list;
		}
}
