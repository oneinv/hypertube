# api

This is documentation for small RestApi with movies and users.

Api contains following methods:
1. **movies**
2. **users**

Books can may take the following keys:

for finding users:

   - 'name' : string,
   - 'lastname' : string
   - 'password' : string(will be hashed),
   - 'email' : string UNIQ,
   - 'login' : string UNIQ

1. **GET**

*example:*

	curl -X GET 'http://localhost:8100/api/users/?name=King&lastname=Steven&login=SKing&email=stefenking@yahoood.com&password=mypass123'

*response:*

	[
		{
			"lastname" : "King",
			"name" : "Test",
			"login" : "Sking",
			"email" : "100",
			"year" : "1987",
			"price" : "120",
			"isbn" : "12346"
		}
	]

2. **POST**

*example:*

	curl -X POST 'http://localhost:8100/api/books/?author=King&name=Test&pages=100&price=120&year=1987&theme=Fantasy&isbn=12346'

*response:*

	{
		"repsone" : {
			"message" : "Book created",
			"code" : 200
		}
	}

3. **PUT**

*example:*

	curl -X PUT 'http://localhost:8100/api/books/?author=King&name=Test&pages=100&price=111&year=1987&theme=Fantasy&isbn=12346'

*response:*

	{
		"repsone" : {
			"message" : "Book Test updated!",
			"code" : 200
		}
	}

4. **DELETE**

*example:*

	curl -X DELETE 'http://localhost:8100/api/books/?author=King&name=Test&pages=100&price=111&year=1987&theme=KEK&isbn=12346'

*response:*

	{
		"repsone" : {
			"message" : "Book Test deleted!",
			"code" : 200
		}
	}

Authors can may take the following keys:
for finding books:

- 'name' : string,
- 'theme' : string,

1. **GET**

*example:*

	curl -X GET 'http://localhost:8100/api/authors/?name=King'

*response:*

	{
		"author" : "king",
		"themes" : [
			"fantasy",
			"horror",
			"thrill"
		]
	}

If something go wrong api returns json with error explanation:

		{
			"repsone":{
				"message" : "This ISBN already exists",
				"code" : 400
			}
		}
