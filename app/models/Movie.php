<?php

namespace App\Models;

use PDO;
use Exception;
use App\Components\Db;

class Movie
{
    public static function sortBy($data, $doing = "")
    {
        if ($doing == "")
            return $data;

        if  (!isset($_SESSION['rating']))
            $_SESSION['rating'] = SORT_ASC;
        if  (!isset($_SESSION['title']))
            $_SESSION['title'] = SORT_ASC;
        if  (!isset($_SESSION['year']))
            $_SESSION['year'] = SORT_ASC;


        $order = $_SESSION[$doing];

        if ($doing == "rating") {
            $key = array_column($data, 'rating');
            array_multisort($key, $order, $data);

            if  ($order == SORT_ASC) {
                $_SESSION['rating'] = SORT_DESC;
            }
            if  ($order == SORT_DESC)
                $_SESSION['rating'] = SORT_ASC;

            return $data;
        }
        if ($doing == "title") {
            $key = array_column($data, 'title');
            array_multisort($key, $order, $data);

            if  ($order == SORT_ASC)
                $_SESSION['title'] = SORT_DESC;
            if  ($order == SORT_DESC)
                $_SESSION['title'] = SORT_ASC;

            return $data;
        }
        if ($doing == "year") {
            $key = array_column($data, 'year');
            array_multisort($key, $order, $data);

            if  ($order == SORT_ASC)
                $_SESSION['year'] = SORT_DESC;
            if  ($order == SORT_DESC)
                $_SESSION['year'] = SORT_ASC;

            return $data;
        }

        return $data;
    }

    public static function saveMovieLink($link, $title)
    {
        $title = end(explode('/', $title));
        preg_match('/.*\d{4}/', $title, $matches);
        $title = current($matches);
        $db = Db::getConnection();
        $sql = 'INSERT INTO movies(link, time, uid, title) VALUES (:link, :time, :uid, :title) ON DUPLICATE KEY UPDATE time=:time';
        $result = $db->prepare($sql);
        $result->bindParam(':link', $link, PDO::PARAM_STR);
        $result->bindParam(':time', time(), PDO::PARAM_INT);
        $result->bindParam(':uid', $_SESSION['user'], PDO::PARAM_INT);
        $result->bindParam(':title', $title, PDO::PARAM_STR);
        $result->execute();
    }
    public static function deleteMovieLink($time = 2592000)
    {
        $timeNow = time();
        $db = Db::getConnection();
        $sql = "SELECT * FROM movies";
        $result =$db->prepare($sql);
        $result->execute();
        $movies = $result->fetchAll(PDO::FETCH_ASSOC);
        foreach ($movies as $value) {
            if ((int)$value['time'] + $time < $timeNow) {
                $str = 'rm -rf ' . $value['link'];
                system($str);
                $sqlDelete = "DELETE FROM movies WHERE link='" . $value['link'] . "'";
                $result = $db->prepare($sqlDelete);
                $result->execute();
            }
        }
    }

    public static function getWatched($userId)
    {
        $db = Db::getConnection();
        $sql = "SELECT title FROM movies WHERE uid = '" . $userId . "'";
        $result =$db->prepare($sql);
        $result->execute();
        $movies = $result->fetchAll(PDO::FETCH_ASSOC);
//        foreach ($movies as $key => $movie) {
//           $movies[$key] = str_replace('/tmp/torrent-stream/', ' ', $movie);
//        }
//        dd($movies);
        return $movies;
    }
}