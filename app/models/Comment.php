<?php

namespace App\Models;

use App\Components\Db;
use PDO;
use Exception;

class Comment
{
    /**
     * @param string $movieId
     * @param string $comment
     * @return mixed
     */
    public static function addComment(string $movieId, int $userId,string $comment)
    {
        $db = Db::getConnection();
        $sql = "INSERT INTO comments(movie_id, user_id, comment) VALUES(:movieId, :userId, :comment)";
        $result = $db->prepare($sql);
        $result->bindParam(':movieId', $movieId, PDO::PARAM_STR);
        $result->bindParam(':comment', $comment, PDO::PARAM_STR);
        $result->bindParam(':userId', $userId, PDO::PARAM_STR);
        return $result->execute();
    }

    public static function getComment(string $movieId)
    {
        $db = Db::getConnection();
        $sql = "SELECT comment, user_id FROM comments WHERE movie_id = :movieId";
        $result = $db->prepare($sql);
        $result->bindParam(':movieId', $movieId, PDO::PARAM_STR);
        $result->execute();
        $comments = $result->fetchAll(PDO::FETCH_ASSOC);

        return $comments;
    }

}