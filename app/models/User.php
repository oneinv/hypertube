<?php

namespace App\Models;

require_once(ROOT.'/app/PHPMailer/PHPMailer.php');
require_once(ROOT.'/app/PHPMailer/Exception.php');
require_once(ROOT.'/app/PHPMailer/SMTP.php');

use Pusher;
use PHPMailer\PHPMailer\PHPMailer;
use App\Components\Db;
use PDO;
use Exception;


class User
{
    public static function notificROFL($user_id, $msg)
    {
        $options = array(
            'cluster' => 'eu',
            'useTLS' => true
        );
        $pusher = new Pusher\Pusher(
            '5363577581d81372e311',
            '697bc56f7842a8f2ce53',
            '775066',
            $options
        );

        $data['message'] = $msg;
        $pusher->trigger($user_id, 'my-action', $data);
    }

    public static function emailValidation($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL))
            ;
        else
            throw new Exception('Wrong format for email!');
        if (Self::checkMailExists($email))
            throw new Exception('Email already exists!');
        return true;
    }

    public static function loginValidation($login)
    {
        if (strlen($login) >= 3 && ctype_alnum($login))
            ;
        else
            throw new Exception('Wrong format for login!');
        if (Self::checkLoginExists($login))
            throw new Exception('Login already exists!');
        return true;
    }

    public static function passwordValidation($password)
    {
        if (strlen($password) >= 6 && ctype_alnum($password))
            return true;
        else
            throw new Exception('Wrong format for password!');
    }

    public static function emailChanging($data)
    {
        if ($data['email_old'] == $data['email'])
            ;
        else
            throw new Exception('Email not match with current!');
        $hello =  Self::emailValidation($data['email_new']);
        if  (!$hello)
            throw new Exception($hello);
        else
            return true ;
    }

    public static function passwordChanging($data)
    {
        if (password_verify($data['password_old'], $data['password']))
            ;
        else
            throw new Exception('Password not match with current!');
        $hello =  Self::passwordValidation($data['password_new']);
        if  (!$hello)
            throw new Exception($hello);
        else
            return true ;
    }

    public static function fill_db($str)
    {
        $db = Db::getConnection();
        $sql = $str;
        $result = $db->prepare($sql);
        $result->execute();
    }

    public static function addPhoto($id, $image)
    {
        $db = Db::getConnection();

        $sql = "UPDATE users
                SET 
                    avatar = :avatar
				WHERE id =:id";
        $result = $db->prepare($sql);
        $result->bindParam(':avatar', $image, PDO::PARAM_STR);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return ($result->execute());
    }

    public static function deletePhoto($id, $image)
    {
        $db = Db::getConnection();
        $sql = "UPDATE users
                SET
                    avatar=:image
                WHERE id=:id";
        $result = $db->prepare($sql);
        $tmp = "default.jpg";
        $result->bindParam(':id', $id, PDO::PARAM_INT);;
        $result->bindParam(':image', $tmp, PDO::PARAM_STR);
        if ($result->execute()) {
            $path = "/public/uploads/users/".$id."/".$image;
            if (unlink(ROOT . $path))
                return true;
            return false;
        }
        return false;
    }

    public static function register($login, $email, $password, $fullname, $soc = [])
    {
        $db = Db::getConnection();
        $token = bin2hex(random_bytes(10));
        if (count($soc))
            $sql = 'INSERT INTO users (login, email, password, fullname, token, social, social_id) VALUES (:login, :email, :password, :fullname, :token, :social, :social_id)';
        else
            $sql = 'INSERT INTO users (login, email, password, fullname, token) VALUES (:login, :email, :password, :fullname, :token)';
        $result = $db->prepare($sql);
        $result->bindParam(':login', $login, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
        $result->bindParam(':fullname', $fullname, PDO::PARAM_STR);
        $result->bindParam(':token', $token, PDO::PARAM_STR);
        if (count($soc)) {
            $result->bindParam(':social', $soc['social'], PDO::PARAM_INT);
            $result->bindParam(':social_id', $soc['social_id'], PDO::PARAM_INT);
            $result->execute();
        }
        $result->execute();

        $id = self::getId($login);
        $path = ROOT . '/public/uploads/users/';

        if (!file_exists($path.$id))
            mkdir($path.$id);

    }


    public static function getId($login)
    {
        $db = Db::getConnection();
        $sql = 'SELECT id from users WHERE login =:login';
        $result = $db->prepare($sql);
        $result->bindParam(':login', $login, PDO::PARAM_STR);
        $result->execute();
        $res = $result->fetch(PDO::FETCH_ASSOC);
        return $res['id'];
    }

    public static function checkConfirm($id)
    {
        $db = Db::getConnection();
        $sql = 'SELECT is_auth FROM users WHERE id =:id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
        $ret = $result->fetch();
        return $ret['is_auth'];
    }

    public static function checkMailExists($email)
    {
        $db = Db::getConnection();

        $sql = 'SELECT COUNT(*) from users WHERE email = :email';
        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->execute();

        if($result->fetchColumn()){
            return true;
        }
        return false;
    }

    public static function checkLoginExists($login)
    {
        $db = Db::getConnection();

        $sql = 'SELECT COUNT(*) from users WHERE login = :login';
        $result = $db->prepare($sql);
        $result->bindParam(':login', $login, PDO::PARAM_STR);
        $result->execute();
        if($result->fetchColumn())
            return true;
        return false;
    }


    public static function checkLog()
    {
        if (isset($_SESSION['user'])) {
            return $_SESSION['user'];
        }
    }

    public static function isGuest()
    {
        if (isset($_SESSION['user']))
            return false;
        return true;
    }

    public static function auth($name)
    {
        $_SESSION['user'] = $name;
    }

    public static function isAva($avatar, $id)
    {
        $db = Db::getConnection();
        $sql = "SELECT avatar FROM users WHERE avatar=:avatar AND id=:id";
        $result = $db->prepare($sql);
        $result->bindParam(':avatar', $avatar['image'], PDO::PARAM_STR);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
        $res = $result->fetch(PDO::FETCH_ASSOC);
        if ($res) {
            return true ;
        }
        return false ;
    }

    public static function updateAvaToDefault($id)
    {
        $db = Db::getConnection();
        $sql = "UPDATE users SET avatar = '/uploads/default.jpg' WHERE id=:id";
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function updatePassword($id, $password)
    {
        $password = password_hash($password, PASSWORD_DEFAULT);
        $db = Db::getConnection();
        $sql = "UPDATE users
			SET password = :password
			WHERE id = :id";
        $result = $db->prepare($sql);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function updateEmail($id, $email)
    {
        $db = Db::getConnection();
        $sql = "UPDATE users
			SET email = :email
			WHERE id = :id";
        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function getUserIdByLogin($login)
    {
        $db = Db::getConnection();
        $sql = "SELECT id from users WHERE login=:login";
        $result = $db->prepare($sql);
        $result->bindParam(':login', $login, PDO::PARAM_STR);
        $result->execute();
        $lol = $result->fetch(PDO::FETCH_ASSOC);
        return $lol;
    }

    public static function getUserByID($id)
    {
        $db = Db::getConnection();
        $sql = "SELECT name from users WHERE id=:id";
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();

        $lol = $result->fetch(PDO::FETCH_ASSOC);
        return $lol;
    }
    public static function getUserLoginByID($id)
    {
        $db = Db::getConnection();
        $sql = "SELECT login from users WHERE id=:id";
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();

        $lol = $result->fetch(PDO::FETCH_ASSOC);
        return $lol;
    }

    public static function getFullUserByID($id)
    {
        $db = Db::getConnection();
        $sql = "SELECT * from users WHERE id=:id";
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
        return $result->fetch(PDO::FETCH_ASSOC);
    }

    public static function getUserbyEmail($email)
    {
        $db = Db::getConnection();
        $sql = "SELECT * from users WHERE email=:email";
        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->execute();

        $lol = $result->fetch();
        return $lol;
    }

    public static function getUserbySocial_id($social_id)
    {
        $db = Db::getConnection();
        $sql = "SELECT * from users WHERE social_id=:social_id";
        $result = $db->prepare($sql);
        $result->bindParam(':social_id', $social_id, PDO::PARAM_STR);
        $result->execute();

        $lol = $result->fetch();
        return $lol;
    }

    public static function getUserByLogin($login)
    {
        $db = Db::getConnection();
        $sql = "SELECT * from users WHERE login=:login";
        $result = $db->prepare($sql);
        $result->bindParam(':login', $login, PDO::PARAM_STR);
        $result->execute();
        $login= $result->fetch();
        return $login;
    }



    public static function editData($id, array $data)
    {
        $db = Db::getConnection();

        $sql = "UPDATE users
			SET name = :name,
			    lastname = :lastname
			WHERE id = :id";
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $data['name'], PDO::PARAM_STR);
        $result->bindParam(':lastname', $data['lastname'], PDO::PARAM_STR);

        return $result->execute();

    }

    public static function getToken($email)
    {
        $db = Db::getConnection();
        $sql = "SELECT token FROM users WHERE email = :email";
        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->execute();
        $ret = $result->fetch();
        return $ret['token'];
    }


    public static function getMail($email)
    {
        $db = Db::getConnection();
        $sql = "SELECT email FROM users WHERE email = :email";
        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->execute();
        $email = $result->fetch();
        return $email['email'];
    }


    public static function activateUser($email)
    {
        $db = Db::getConnection();
        $sql = "UPDATE users SET is_auth = 1 WHERE email = :email";
        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        return $result->execute();
    }

    public static function sendMail($email, $token = false, $doing)
    {
        $HTTP_HOST = $_SERVER['HTTP_HOST'];
        $user_mail = $email;
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->Host="smtp.gmail.com";
        $mail->SMTPDebug = 3;
        $mail->SMTPAuth= true;
        $mail->Username ="testingcamagru@gmail.com";
        $mail->Password = "ZZ002191";
        $mail->From="testingcamagru@gmail.com";
        $mail->FromName= "no-reply@camagru.com";
        $mail->addAddress($user_mail, "HELLO");
        $mail->isHTML(true);
        if ($doing == "activate") {
            $mail->Subject = "Please verify your account email";
            $mail->Body = "
		Please click on the link below to confirm your registration: <br><br>
		<a href='http://$HTTP_HOST/user/account/confirm/$user_mail&$token'>Click Here</a>";
        }
        else if ($doing =="recover") {
            $mail->Subject = "Recover password";
            $mail->Body = "
			Please click on the link below to Recover your password on hypertube: <br><br>
			<a href='http://$HTTP_HOST/user/account/recover/$user_mail&$token'>Click Here</a>";
        }

        $mail->send();
    }

    public static function getAllUsers()
    {
        $db = Db::getConnection();
        $sql = "SELECT * FROM users";
        $result =$db->prepare($sql);
        $result->execute();
        $i = 0;
        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $ids[$i] = $row;
            $i++;
        }
        return $ids;
    }

    public static function sortBy($data, $doing = "")
    {
        if ($doing == "")
            return $data;
        if ($doing == "age") {
            $age = array_column($data, 'age');
            array_multisort($age, SORT_DESC, $data);
            return $data;
        }
        if ($doing == "fame") {
            $fame = array_column($data, 'fame');
            array_multisort($fame, SORT_DESC, $data);
            return $data;
        }
        if ($doing == "tags") {
            $count = array_column($data, 'count');
            array_multisort($count, SORT_DESC, $data);
            return $data;
        }
        if ($doing == "location") {
            $geo = array_column($data, 'geo');
            array_multisort($geo, SORT_DESC, $data);
            return $data;
        }
        return $data;
    }

    public function native($userId)
    {
        $db = Db::getConnection();
        $sql = "SELECT password FROM users WHERE id =:id";
        $result =$db->prepare($sql);

        $result->bindParam(':id', $userId, PDO::PARAM_INT);
        $result->execute();
        $row = $result->fetch(PDO::FETCH_ASSOC);
//        dd($row['password']);
        if ($row['password'] == '0') {
            return false;
        }
        return true;
    }


}
