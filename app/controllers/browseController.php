<?php

namespace App\Controllers;

use Slim\Views\Twig as View;
use App\Models\User;
use App\Models\Comment;
use App\Components\YTS;
use App\Components\Imdb;
use App\Models\Movie;
use App\Auth\Auth;

class BrowseController extends Controller
{
    public function index($request, $response)
    {
        return $this->container->view->render($response, '/main/find.twig');
    }

    public function get_films($request, $response)
    {
        $list = [];
        $data  = $request->getParams();
        if (isset($data['minimum']) && ($data['minimum'] > 10 || $data['minimum'] < 0)) {
            $data['minimum'] = 0;
        }

        if (isset($data['action']) && ($data['action'] == "main" || $data['action'] == "search"))
        {
            $yts = new YTS();
            if ($data['action'] == "main")
                $movies = $yts->listMovies('all', 50);
            else if ($data['action'] == "search")
                $movies = $yts->listMovies('all', 50,
                    strtr($data['word'], [' ' => '%20']), 0,  round(floatval($data['minimum']), 1),
                    $data['genres']);
            $i = 0;
            foreach ($movies as $movie) {
                $list[$i]['title'] = (string)$movie->title;
                $list[$i]['rating'] = (float)$movie->rating;
                $list[$i]['synopsis'] = $movie->synopsis;
                if (strlen($list[$i]['synopsis']) >= 150) {
                    $list[$i]['synopsis'] = substr($list[$i]['synopsis'], 0, 150);
                    $list[$i]['synopsis']= substr_replace($list[$i]['synopsis'], "...", 150);
                }
                $list[$i]['poster'] = $movie->medium_cover_image;
                $list[$i]['year'] = $movie->year;
                $list[$i]['imdb_code'] = $movie->imdb_code;
                foreach ($movie->torrents as $key => $value) {
                    $list[$i][$value->quality] = (string)$value->hash;
                }
                $i++;
            }
            array_multisort(array_column($list, 'rating'), SORT_DESC, $list);
        }
        else if (isset($data['action'], $data['param'], $data['res']) && $data['action'] == 'sort')
        {
            $list = Movie::sortBy($data['res'], $data['param']);
            $this->container->view->getEnvironment()->addGlobal('data', $list);
        }
        echo json_encode($list);
        exit();
    }

    public function getMainPage($request, $response, $args)
    {
        $list = array();
        $title = strtr($args['movie'], [' ' => '%20']);

        $yts = new YTS();
        $movie = $yts->listMovies('all', 1, $title);
        $list['title'] = $movie[0]->title_long;
        $list['rating'] = $movie[0]->rating;
        $list['synopsis'] = $movie[0]->synopsis;
        $list['id'] = $movie[0]->imdb_code;
        $list['year'] = $movie[0]->year;
        $list['background'] = $movie[0]->background_image;
        $list['genres'] = implode(', ', $movie[0]->genres);
        $list['language'] = $movie[0]->language;

        $list['poster'] = $movie[0]->medium_cover_image;
        foreach ($movie[0]->torrents as $key => $value) {
            $list['torrents'][$value->quality] = (string)$value->hash;
        }
        
        $comments = Comment::getComment($list['id']);
        foreach ($comments as $key => $comment) {
            if (User::getUserLoginByID($comment['user_id'])['login'] == '0') {
                $login['login'] = "Inner user";
            } else {
                $login = User::getUserLoginByID($comment['user_id']);
            }
            $comments[$key]['user_id'] = $login['login'] ;
        }
        $this->container->view->getEnvironment()->addGlobal('movie', $list);
        $this->container->view->getEnvironment()->addGlobal('comments', $comments);

        return $this->container->view->render($response, '/main/page.twig');
    }

    public function postMainPage($request, $response, $args)
    {
        $movieId = $args['movie'];
        if (strlen($movieId) > 50) {
            $this->container->flash->addMessage('info', 'go fuck yourself' );
            return $response->withRedirect($this->container->router->pathFor('home'));
        }
        $userId = $_SESSION['user'];
        if (isset($_POST['submit'])) {
            $comment = ($_POST['comment']);
            if (strlen($_POST['comment']) == 0 || strlen($_POST['comment']) > 150)
                $comment = 'try more';
            Comment::addComment($movieId, $userId, $comment);
        }

        return $response->withRedirect($this->container->router->pathFor('find.next',
            ['movie' => $args['movie']]));
    }

    public function getLink($request, $response)
    {
        $data  = $request->getParams();
        $title = strtr($data['name'], [' ' => '%20']);
        $quality = $data['quality'];

        $yts = new YTS();
        $movie = $yts->listMovies('all', 1, $title);

        foreach ($movie[0]->torrents as $key => $value) {
            $list[$value->quality]['torrents'] = (string)$value->hash;
            $list[$value->quality]['size'] = (string)$value->size;
        }

        $res = array(
            'hash' => $list[$quality.'p']['torrents'],
            'size' => $list[$quality.'p']['size']
        );
        echo json_encode($res);
        exit();

    }
    public function getTrailers($request, $response, $args)
    {
        $list = array();
        $id = $args['movie'];

        $token  = new \Tmdb\ApiToken('a1925731688fff640e557ac8e3f80bd8');
        $client = new \Tmdb\Client($token);

        $movie = $client->getMoviesApi()->getTrailers($id);
        foreach ($movie['youtube'] as $source) {
            $videos[] = $source['source'];
        }
        if (!count($videos))
            return $response->withStatus(404)
                ->withHeader('Content-Type', 'text/html')
                ->write('Page not found');

        $this->container->view->getEnvironment()->addGlobal('id', $id);
        $this->container->view->getEnvironment()->addGlobal('videos', $videos);

        return $this->container->view->render($response, '/main/trailers.twig');

    }

    public function createLinkTime($request, $response)
    {
        $data  = $request->getParams();
        if (isset($data['hashFolder'])) {
            Movie::saveMovieLink($data['hashFolder'], $data['link']);
            echo true;
        } else {
            echo false;
        }
        exit();
    }

    public function getWatched($request, $response)
    {
        $videos = Movie::getWatched((int)$_SESSION['user']);

        $this->container->view->getEnvironment()->addGlobal('videos', $videos);

        return $this->container->view->render($response, '/main/watched.twig');
    }

}

