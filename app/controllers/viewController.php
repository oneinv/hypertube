<?php

namespace App\Controllers;

use Slim\Views\Twig as View;
use App\Models\User;

class ViewController extends Controller
{
    public function index($request, $response, $args)
    {

        return $this->container->view->render($response, 'home.twig');
    }

    public function getMainPage($request, $response, $args)
    {
        if ($args['login'] === '0') {
            $this->container->flash->addMessage('info', $this->text['its']);
            return $response->withRedirect($this->container->router->pathFor('home'));
        }

        $user_data = User::getUserByLogin($args['login']);
        if ($user_data === false) {
            $this->container->flash->addMessage('info', $this->text['no_user']);
            return $response->withRedirect($this->container->router->pathFor('home'));
        }
        $id = $_SESSION['user'];
        $user_id = User::getUserIdByLogin($args['login']);

        $this->container->view->getEnvironment()->addGlobal('user_data', $user_data);

        return $this->container->view->render($response, '/users/page.twig');
    }

    public function postMainPage($request, $response, $args)
    {
        $user_data = User::getUserByLogin($args['login']);
        $id = $_SESSION['user'];
        $doing = $request->getParams();

        return $response->withRedirect($this->container->router->pathFor('find.next_user',
            ['login' => $args['login']]));
    }


}
