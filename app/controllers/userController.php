<?php

namespace App\Controllers;

use Slim\Views\Twig as View;
use App\Models\User;
//use App\Models\Photo;
//use App\Models\Location;
//use App\Models\Tags;
//use App\Models\Online;
//use App\Models\Like;
//use App\Models\Views;
use App\Auth\Auth;
use Exception;

class UserController extends Controller
{

    public function getSignOut($request, $response)
    {
        $this->container->auth->logout();
        return $response->withRedirect($this->container->router->pathFor('home'));
    }

    public function getSignIn($request, $response)
    {
        return $this->container->view->render($response, '/user/signin.twig');
    }

    public function loginIntra($request, $response)
    {
        $_GET = $request->getParams();
        if (isset($_GET['code']))
        {
            $params = [
                'grant_type'	=> 'authorization_code',
                'client_id'		=> 'da0b6d196c7299e826ab63646a644820fd662c99a57424f32b3603c237edaf6a',
                'client_secret'	=> 'b1fb5c78686ada59e3c34ca37bda0be18ea01f4383d0a5b173124b078631be19',
                'code'			=> $_GET['code'],
                'redirect_uri'	=> 'http://localhost:8100/social/intra',
            ];

            $result = json_decode($this->performCurl('https://api.intra.42.fr/oauth/token', $params), true);
            if (isset($result['error']))
                return $response->withRedirect('/');
            $end = json_decode($this->performCurl('https://api.intra.42.fr/v2/me', false, ['Authorization: Bearer '.$result['access_token']]), true);
            if ($end)
            {
                if (!$this->container->auth->attempt_social(2, $end['id']))
                {
                    User::register(0, $end['email'], 0, $end['displayname'], ['social' => 2, 'social_id' => $end['id']]);
                    $this->container->auth->attempt_social(2, $end['id']);
                }
                return $response->withRedirect($this->container->router->pathFor('home'));
            }
        }
        else
        {
            $params = [
                'client_id'		=> 'da0b6d196c7299e826ab63646a644820fd662c99a57424f32b3603c237edaf6a',
                'redirect_uri'	=> 'http://localhost:8100/social/intra',
                'response_type'	=> 'code'
            ];
            return $response->withRedirect('https://api.intra.42.fr/oauth/authorize'.'?'.http_build_query($params));
        }
    }

    public function loginFacebook($request, $response)
    {

        $fb = new \Facebook\Facebook([
            'app_id' => '314194756176437',
            'app_secret' => 'd97c16bec60e0566cb9a54663c1f9aae',
            'default_graph_version' => 'v2.10'
        ]);

        $helper = $fb->getRedirectLoginHelper();

        try
        {
            $accessToken = $helper->getAccessToken();
        }
        catch(Facebook\Exceptions\FacebookResponseException $e)
        {
            echo 'Graph returned an error: ' . $e->getMessage();
            return false;
        }
        catch(Facebook\Exceptions\FacebookSDKException $e)
        {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            return false;
        }

        if (isset($accessToken))
        {
            $fb->setDefaultAccessToken($accessToken);
            try
            {
                $res = $fb->get('/me?fields=email,name');
                $end = $res->getGraphUser();
            }
            catch(Facebook\Exceptions\FacebookResponseException $e)
            {
                echo 'Graph returned an error: ' . $e->getMessage();
                return false;
            }
            catch(Facebook\Exceptions\FacebookSDKException $e)
            {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                return false;
            }
            if (!($end->getProperty('email')))
                $email = 0;
            else
                $email = $end->getProperty('email');

            if (!$this->container->auth->attempt_social(1, $end->getId()))
            {
                User::register(0, $email, 0, $end->getName(), ['social' => 1, 'social_id' => $end->getId()]);
                $this->container->auth->attempt_social(1, $end->getId());
            }
            return $response->withRedirect($this->container->router->pathFor('home'));
        }
        else
        {
            $loginUrl = $helper->getLoginUrl('http://localhost:8100/social/facebook', ['email']);
            return $response->withRedirect($loginUrl);
        }
    }

    public function postSignIn($request, $response)
    {
        $info = $request->getParams();
        if (isset($info['forgot'])) {
            $email = $request->getParam('email');
            if (strlen($email) > 50) {
                $this->container->flash->addMessage('info', $this->text['val_50'] );
                return $response->withRedirect($this->container->router->pathFor('home'));
            }
            if (User::checkMailExists($email)) {
                $token = User::getToken($email);
                User::sendMail($email, $token, "recover");
                $this->container->flash->addMessage('info', $this->text['val_resend_email']);
                return $response->withRedirect($this->container->router->pathFor('home'));
            } else {
                $this->container->flash->addMessage('error', $this->text['val_such_email']);
                return $response->withRedirect($this->container->router->pathFor('user.signin'));
            }
        } elseif (isset($info['g-recaptcha-response'])) {
            $url_to_google_api = "https://www.google.com/recaptcha/api/siteverify";

            $secret_key = '6LdFjW8UAAAAAEySyJXDTiWhheKuKyj8tj2OjhFm';
            $query = $url_to_google_api . '?secret=' . $secret_key . '&response='
                . $_POST['g-recaptcha-response'] . '&remoteip=' . $_SERVER['REMOTE_ADDR'];
            $data = json_decode(file_get_contents($query));

            $email = $request->getParam('email');
            $password = $request->getParam('password');
            if (strlen($email) > 50 || strlen($password) > 50) {
                $this->container->flash->addMessage('info', $this->text['val_50'] );
                return $response->withRedirect($this->container->router->pathFor('home'));
            }

            if ($data->success) {
                $auth = $this->container->auth->attempt(
                    $request->getParam('email'),
                    $request->getParam('password')
                );
                if (!$auth) {
                    $this->container->flash->addMessage('error', $this->text['wrong_data']);
                    return $response->withRedirect($this->container->router->pathFor('user.signin'));
                } else {
                    return $response->withRedirect($this->container->router->pathFor('home'));
                }
            } else {
                $this->container->flash->addMessage('error', $this->text['valid_captcha']);
                return $response->withRedirect($this->container->router->pathFor('user.signin'));
            }
        }
    }

    public function getSignUp($request, $response)
    {
        return $this->container->view->render($response, '/user/signup.twig');
    }

    public function postSignUp($request, $response)
    {
        $data = $request->getParams();

        if (strlen($data['email']) > 50 || strlen($data['password']) > 50 || strlen($data['login']) > 50) {
            $this->container->flash->addMessage('info', $this->text['val_50'] );
            return $response->withRedirect($this->container->router->pathFor('home'));
        }

        $errors = $this->catchErrors($data);
        if (count($errors)) {
            $this->container->view->getEnvironment()->addGlobal('errors', $errors);
            return $this->container->view->render($response, '/user/signup.twig');
        }
        array_map('htmlspecialchars', $data);
        $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
        User::register($data['login'], $data['email'], $data['password'], $data['lang']);
        // как бы работает автологин но нужно подумать что делать с
        // окном заполнения всех полей. так что пока отключен
        // $this->container->auth->attempt($user['email'], $pswd);
        $this->container->flash->addMessage('info', $this->text['succ_reg']);
        return $response->withRedirect($this->container->router->pathFor('home'));
    }

    public function getChangeEmail($request, $response)
    {
        return $this->container->view->render($response, '/user/mail.twig');
    }

    public function postChangeEmail($request, $response)
    {
        $user = ($this->container->auth->user());
        $errors = array();
        $data = array();
        $data['email'] = $user['email'];
        $data['email_old'] = $request->getParam('email_old');
        $data['email_new'] = $request->getParam('email_new');

        if (strlen($data['email_old']) > 50 || strlen($data['email_new']) > 50 || strlen($data['email']) > 50) {
            $this->container->flash->addMessage('info', $this->text['val_50'] );
            return $response->withRedirect($this->container->router->pathFor('home'));
        }

        $errors = $this->catchChange("email", $data);
        if (count($errors) !== 0) {
            $this->container->view->getEnvironment()->addGlobal('errors', $errors);
            return $this->container->view->render($response, '/user/mail.twig');
        } else {
            User::updateEmail($user['id'], $data['email_new']);
            $this->container->flash->addMessage('info', $this->text['succ']);
            return $response->withRedirect($this->container->router->pathFor('home'));
        }
    }

    public function getChangePassword($request, $response)
    {
        return $this->container->view->render($response, '/user/password.twig');
    }

    public function postChangePassword($request, $response)
    {
        $user = ($this->container->auth->user());
        $errors = array();
        $data = array();
        $data['password'] = $user['password'];
        $data['password_old'] = $request->getParam('password_old');
        $data['password_new'] = $request->getParam('password_new');

        if (strlen($data['password']) > 50 || strlen($data['password_old']) > 50 || strlen($data['password_new']) > 50) {
            $this->container->flash->addMessage('info', $this->text['val_50'] );
            return $response->withRedirect($this->container->router->pathFor('home'));
        }
        $errors = $this->catchChange("password", $data);

        if (count($errors) !== 0) {
            $this->container->view->getEnvironment()->addGlobal('errors', $errors);
            return $this->container->view->render($response, '/user/password.twig');
        } else {
            User::updatePassword($user['id'], $data['password_new']);
            $this->container->flash->addMessage('info', $this->text['succ']);
            return $response->withRedirect($this->container->router->pathFor('home'));
        }
    }

    public function getChangeData($request, $response)
    {
        $user_data = User::getFullUserByID($_SESSION['user']);

        $this->container->view->getEnvironment()->addGlobal('user_data', $user_data);
        return $this->container->view->render($response, '/user/accountchange.twig');
    }

    public function postChangeData($request, $response)
    {
        $user = $request->getParams();

        if (strlen($user['name']) > 50 || strlen($user['lastname']) > 50) {
            $this->container->flash->addMessage('info', $this->text['val_50'] );
            return $response->withRedirect($this->container->router->pathFor('home'));
        }

        User::editData($_SESSION['user'], $user);
        return $response->withRedirect($this->container->router->pathFor('home'));
    }

    public function getData($request, $response)
    {
        $user_data = User::getFullUserByID($_SESSION['user']);
        unset($user_data['password'], $user_data['token'], $user_data['is_auth']);

        $this->container->view->getEnvironment()->addGlobal('user_data', $user_data);

        return $this->container->view->render($response, '/user/account.twig');
    }

    public function getPhoto($request, $response)
    {
        return $this->container->view->render($response, '/user/accountphoto.twig');
    }

    public function postPhoto($request, $response)
    {
        $userId = $_SESSION['user'];
        $ok = array("image/png", "image/jpeg", "image/jpg", "image/gif");
        $image = mb_strtolower($_FILES['image']['name']);
        $path = getcwd();
        $target = $path . "/uploads/users/" . $_SESSION['user'] . '/' . $image;
        if (User::addPhoto($userId, $image)) {
            if($rofl = move_uploaded_file($_FILES['image']['tmp_name'], $target)){
                $type = mime_content_type($target);
                if (in_array($type, $ok)){
                    $this->container->flash->addMessage('info', $this->text['succ']);
                } else {
                    User::deletePhoto($image, $userId);
                    $this->container->flash->addMessage('error', $this->text['wrong_format']);
                }
            } else {
                echo "not " . $_FILES["file"]["error"];
            }
        } else {
            $this->container->flash->addMessage('error', $this->text['val_5_photo']);
        }
        return $response->withRedirect($this->container->router->pathFor('user.account'));
    }

    public function getToken($request, $response, $args)
    {
        $email = $args['email'];
        $token = $args['token'];

        if (strlen($email) > 50 || strlen($token) > 50) {
            $this->container->flash->addMessage('info', $this->text['val_50'] );
            return $response->withRedirect($this->container->router->pathFor('home'));
        }
        $TrueToken = User::getToken($email);
        $TrueEmail = User::getMail($email);
        if($TrueEmail == $email && $TrueToken == $token){
            $this->container->flash->addMessage('info', $this->text['profile_active']);
        }
        else {
            $this->container->flash->addMessage('error', $this->text['wrong']);
        }
        return $response->withRedirect($this->container->router->pathFor('home'));
    }

    public function getRecover($request, $response, $args)
    {
        $email = $args['email'];
        $token = $args['token'];

        if (strlen($email) > 50 || strlen($token) > 50) {
            $this->container->flash->addMessage('info', $this->text['val_50'] );
            return $response->withRedirect($this->container->router->pathFor('home'));
        }
        $TrueToken = User::getToken($email);
        $TrueEmail = User::getMail($email);
        if($TrueEmail == $email && $TrueToken == $token){
            $this->container->flash->addMessage('info', $this->text['not_change_pass']);
            return $response->withRedirect($this->container->router->pathFor('user.recover',
                ['email' => $email]));
        } else {
            $this->container->flash->addMessage('error', $this->text['wrong']);
            return $response->withRedirect($this->container->router->pathFor('user.signin'));
        }
    }

    public function postRecover($request, $response, $args)
    {
        $data['email'] = $args['email'];
        $data['password'] = $request->getParam('password');

        if (strlen($data['email']) > 50 || strlen($data['token']) > 50) {
            $this->container->flash->addMessage('info', $this->text['val_50'] );
            return $response->withRedirect($this->container->router->pathFor('home'));
        }
        $user = User::getUserByEmail($data['email']);
        $errors = $this->catchChange("recover", $data);
        if (count($errors) !== 0) {
            $this->container->view->getEnvironment()->addGlobal('errors', $errors);
            $this->container->view->getEnvironment()->addGlobal('email', $user['email']);
            return $this->container->view->render($response, '/user/recover.twig');
        } else {
            User::updatePassword($user['id'], $data['password']);
            $this->container->flash->addMessage('info', $this->text['succ']);
            return $response->withRedirect($this->container->router->pathFor('home'));
        }
    }

    private function catchErrors($data)
    {
        $errors = array();

        try {
            $email = $this->validate("email", $data['email']);
        } catch (Exception $e) {
            $errors['email'] = $e->getMessage();
        }
        try {
            $login = $this->validate("login", $data['login']);
        } catch (Exception $e) {
            $errors['login'] = $e->getMessage();
        }
        try {
            $password = $this->validate("password", $data['password']);
        } catch (Exception $e) {
            $errors['password'] = $e->getMessage();
        }
        return $errors;
    }

    private function catchChange($field, $data)
    {
        $errors = array();
        if ($field == "email") {
            try {
                $email_new = $this->validate("emailMatch", $data);
            } catch (Exception $e) {
                $errors['email_new'] = $e->getMessage();
            }
        } elseif ($field == "password") {
            try {
                $password_new = $this->validate("passwordMatch", $data);
            } catch (Exception $e) {
                $errors['password_new'] = $e->getMessage();
            }
        } else if ($field == "recover") {
            try {
                $password = $this->validate("password", $data['password']);
            } catch (Exception $e) {
                $errors['password'] = $e->getMessage();
            }
        }
        return $errors;
    }

    private function validate($field, $data)
    {
        switch ($field) {
            case "email":
                return User::emailValidation($data);
            case "login":
                return User::loginValidation($data);
            case "password":
                return User::passwordValidation($data);
            case "emailMatch":
                return User::emailChanging($data);
            case "passwordMatch":
                return User::passwordChanging($data);
        }
        return false;
    }

    public function ChangeLang($request, $response)
    {
        if ($_SESSION['lang'] == 'en')
            $_SESSION['lang'] = 'ru';
        else if ($_SESSION['lang'] == 'ru')
            $_SESSION['lang'] = 'en';
        return $response->withRedirect($_SERVER['HTTP_REFERER']);
    }


}
