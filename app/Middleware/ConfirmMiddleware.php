<?php

namespace App\Middleware;

class ConfirmMiddleware extends Middleware
{
	public function __invoke($request, $response, $next)
	{
		if (!$this->container->auth->is_auth()) {
			$this->container->flash->addMessage('error', 'You must confirm your mail');
			return $response->withRedirect($this->container->router->pathFor('home'));
		}

		$response = $next($request, $response);
		return $response;
	}
}
