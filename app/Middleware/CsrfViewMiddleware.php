<?php

namespace App\Middleware;

class CsrfViewMiddleware extends Middleware
{
	public function __invoke($request, $response, $next)
	{
        $path = (array)$request->getAttribute('route');

        $array = array_values($path);

        $route = $array[2];

        if ($route == 'get.link') {
            $response =  $next($request, $response);
            return $response;
        }
        else {
            $this->container->view->getEnvironment()->addGlobal('csrf', [
                'field' => '
				<input type="hidden" name="' . $this->container->csrf->getTokenNameKey() . '"
					value="' . $this->container->csrf->getTokenName() . '">
				<input type="hidden" name="' . $this->container->csrf->getTokenValueKey() . '"
					value="' . $this->container->csrf->getTokenValue() . '">
			',
            ]);

            $response = $next($request, $response);
            return $response;
        }
	}
}
