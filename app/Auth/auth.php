<?php


namespace App\Auth;

use App\Models\User;
//use App\Models\Ignore;

class Auth
{
    public function user()
    {
        return User::getFullUserByID($_SESSION['user']);
    }

    public function check()
    {
        return isset($_SESSION['user']);
    }

    public function native()
    {
        return User::native($_SESSION['user']);
    }

    public function is_auth()
    {
        return User::checkConfirm($_SESSION['user']);
    }

    public function attempt_social($social, $social_id)
    {
        $user = User::getUserbySocial_id($social_id);
        if ($social_id == $user['social_id'] && $social == $user['social']) {
            $_SESSION['user'] = $user['id'];
            return true;
        }
        return false;
    }


    public function attempt($email, $password)
    {
        if (!$user = User::checkMailExists($email))
            return false;
        $user = User::getUserbyEmail($email);
        if (password_verify($password, $user['password'])) {
            $_SESSION['user'] = $user['id'];
            return true;
        }
        return false;
    }

    public function logout()
    {
        unset($_SESSION['user']);
    }
}
